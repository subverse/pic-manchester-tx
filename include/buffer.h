#ifndef __BUFFER_H__
#define __BUFFER_H__

#define BUFSIZ				20

/////////////////////////////////////////////////////////
//
//	BUFFER PROTOTYPES
//
/////////////////////////////////////////////////////////
char bufferInitialise( struct queue * );
char bufferGet( char *, struct queue * );
char bufferPut( char, struct queue * );

/////////////////////////////////////////////////////////
//
//	Buffer Header Structure
//
/////////////////////////////////////////////////////////
struct queue
{
	char buffer[BUFSIZ];	//buffer
	char head;				//index to head of buffer
	char tail;				//index to tail of buffer
};

#endif
