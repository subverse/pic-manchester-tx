/////////////////////////////////////////////////////////
//
//Name:			lcd.c
//
//Author:		David McKelvie
//
//Version:		1.2.1 02/08/2004
//				*cleaned up, condensed, some PIC
//				 optimisation.
//
//				PORTING TO PIC16F628!!!
//				1.2 04/03/2003
//
//History		*Added serial lcd functionallity
//
//				1.1	30/08/2002
//				*Removed 'line + offset' argument from
//				 lcdPrint function, replaced with
//				 single 'position'.
//				*Renamed functions to be more like 
//				 standard file IO.
//				*Replaced unsigned char with char
//				 defined in my68hc11.h.
//				*Added lcdScroll();
//
//				1.0 14/05/2002
//				Initial Release.
//
//Description:	HD44870 type lcd driver
//
//Functions:	lcdOpen();			//initialise lcd
//				lcdIoctl();			//write command
//				lcdWrite();			//write char
//				lcdPrint();			//write string
//				lcdScroll();
//				lcdSetCGRam();		//see function header
//				lcdBarInitialise()	//setup bargraph characters
//				lcdSetBargraph()	//fill buffer for bargraph
//
//Basic Usage:	Call lcdOpen(),
//
//				Call lcdPrint( s, a ), where s is
//				a pointer to a '\0' terminated character
//				array (string), a is the address
//				in DD ram where you wish to write s,
//				two possible base addresses are LINE_ONE
//				and LINE_TWO, defined in LCD.H
//
//
/////////////////////////////////////////////////////////
#include<pic.h>
#include"mypic.h"
#include"lcd.h"

bit doubleWrite;

/////////////////////////////////////////////////////////
//
//Name:			lcdOpen
//
//Description:	Initialises lcd
//
//Name:			lcdOpen
//
//Description:	Initialises lcd display according to Hitachi
//				recommendations for software initialisation
//				of HD44870 lcd controllers.	
//
//				
/////////////////////////////////////////////////////////
void lcdOpen( void )
{
	doubleWrite = FALSE;
	TRISB = 0x01;
	RBPU = 0;
	lcdIoctl( LCD_8_BIT );					//function set
	lcdIoctl( LCD_8_BIT );					//function set
	lcdIoctl( LCD_8_BIT );					//function set
	lcdIoctl( LCD_4_BIT );					//4 line interface
	doubleWrite = TRUE;						//enable double write
	lcdIoctl( LCD_4_BIT | LCD_2_LINE );
	lcdIoctl( LCD_DISPLAY_OFF );			//display off
	lcdIoctl( LCD_CLEAR_DISPLAY );			//clear display
	lcdIoctl( LCD_INCREMENT_DD_RAM );		//entry mode set
	lcdIoctl( LCD_DISPLAY_ON | LCD_CURSOR_OFF | LCD_BLINK_OFF );	//display on
}

/////////////////////////////////////////////////////////
//
//Name:			lcdIoctl
//
//Description:	writes command byte to lcd Instruction
//				Register by setting state of RS to 0,
//				then calling lcdWrite() with input
//				'command' before resetting the state of
//				RS to 1.
//
/////////////////////////////////////////////////////////
void lcdIoctl( char command )
{
	if( doubleWrite )
	{
		DATA_LINES = command & 0xf0;
		RS = RS_INSTRUCTION;
		E = E_ENABLE;
		delay( 2 );
		E = E_DISABLE;
		DATA_LINES = command << 4;
		RS = RS_INSTRUCTION;
		E = E_ENABLE;
		delay( 2 );
		E = E_DISABLE;
	}
	else
	{
		DATA_LINES = command;
		RS = RS_INSTRUCTION;
		E = E_ENABLE;
		delay( 2 );
		E = E_DISABLE;
	}
	delay( 100 );
}

/////////////////////////////////////////////////////////
//
//Name:			lcdWrite
//
//Description:	Depending on the value of global
//				'numDataLines' lcdWrite will do one of
//				the following:
//
//					*write a byte to lcd data lines 
//					 in 4 bit mode.(double write).
//
//					*write a byte to lcd data lines 
//					 in 8 bit mode.(single write).
//
//											
//				supports the use of Bit 7
//				of port as MSB in 8 or 4 bit mode.
//
//
//////////////////////////////////////////////////////////
void lcdWrite( char lcdData )
{
	if( doubleWrite )
	{
		DATA_LINES = lcdData & 0xf0;
		RS = RS_DATA;
		E = E_ENABLE;
		delay(2);
		E = E_DISABLE;
		DATA_LINES = lcdData << 4;
		RS = RS_DATA;
		E = E_ENABLE;
		delay(2);
		E = E_DISABLE;
	}
	else
	{		
		DATA_LINES = lcdData;
		RS = RS_DATA;
		E = E_ENABLE;
		delay(2);
		E = E_DISABLE;
	}
	delay( 50 );
}

/////////////////////////////////////////////////////////
//
//Name:			lcdPrint
//
//Description:	lcd fprintf type funtion, sends a string
//				of characters to the lcd starting at the 
//				DD ram address given by input 'position'
//
//				Two possible values for 'position' include
//				LINE_ONE and LINE_TWO #defined in lcd.h
//				as 0x80 and 0xC0 respectively.
//
//				Writes characters until the terminating
//				'\0' is encountered.
//
/////////////////////////////////////////////////////////
void lcdPrint( const char *string, char position )
{
	char index;
	index = 0;

	lcdIoctl( position );				//set DD RAM address
	
	//write until '\0'. Trap serial mode command escape
	//character 0xFF
	while( string[ index ] != '\0' )	
		lcdWrite( string[ index++ ] );
}

/////////////////////////////////////////////////////////
//
//Name:			lcdScroll
//
//Description:	scrolls a string of text on the display
//
//				Call with pointer to the text string to
//				be displayed.
//
//
/////////////////////////////////////////////////////////
char lcdScroll( const char *string )
{
	char i;
		
	lcdIoctl( LCD_CLEAR_DISPLAY );
	
	for( i = 0; i < LCD_COLUMNS; i ++ )				
		lcdIoctl( LCD_SHIFT_DISPLAY_RIGHT );
	
	for( i = 0; i < 0xff; i ++ )
	{
		if( i == 40 || i == 80 )					//wrap text
			lcdIoctl( LCD_LINE_ONE );					//set to start	
				
		lcdWrite( string[ i ] );					//output character
		lcdIoctl( LCD_SHIFT_DISPLAY_LEFT );			//shift display
		delay( 15000 );
				
		if( string[ i + 1 ] == '\0' )				//last character?
		{
			for( i = 0; i < LCD_COLUMNS; i ++ )		//scroll off display
			{
				lcdWrite( ' ' );					//clear first few chars
				lcdIoctl( LCD_SHIFT_DISPLAY_LEFT );
				delay( 15000 );
			}
			return TRUE;	
		}
	}
	return TRUE;
}

/////////////////////////////////////////////////////////
//
//Name:			lcdSetCGRam
//
//Description:	Sets character generator pattern in CG ram.
//
//				Leaves AC (Address Counter) at 'cgAddress + 8'
//				so AC must be reset after use. 
//				To reduce interface lines, W/R of lcd is 
//				tied low and therefore AC cannot be 
//				read, saved and restored.
//
//Inputs:		cgAddress, address in CG ram to place custom
//				pattern, 0 - 7.
//
//				bitPattern, pointer to char array containing
//				bit pattern of custom character.
//
//				BIT PATTERN		EG			HEX
//				76543210			
//DATA BYTE		---XXXXX	    XXXX		1E
//				---XXXXX	    X   X		11
//				---XXXXX	    X   X		11
//				---XXXXX	    XXXX		1E
//				---XXXXX	    X   X		11
//				---XXXXX	    X   X		11
//				---XXXXX	    XXXX		1E
//				
//				CODE SAMPLE
//				 .
//				 .
//				//define character
//				char letter_B[] = { 0x1E, 0x11, 0x11, 0x1E, 0x11, 0x11, 0x1E };	
//				lcdSetCGRam( LCD_CG_RAM_ONE, letter_B );			//set character
//				lcdWrite( LCD_CG_RAM_ONE );							//display character
//				 .
//				 .
//
//Notes:		Avoid the use of CG ram address 0 when
//				using lcdPrint() to output strings containing
//				CG characters as lcdPrint() stops writing
//				to lcd when it encounters '\0' in a string.
//				Use lcdWrite() to output single character 
//				in this case.
//
/////////////////////////////////////////////////////////
void lcdSetCGRam( char cgAddress, char *bitPattern )
{
	char i;

	lcdIoctl( LCD_SET_CG_RAM | cgAddress );	//set CG ram address

	for( i = 0; i < 8; i ++ )						//write pattern
		lcdWrite( bitPattern[ i ] );
}

////////////////////////////////////////////////////////
//
//Name:			barInitialise
//
//Description:	Sets up bargraph characters in CG ram
//				using patterns declared and defined
//				in bargraph.h
//
//				Avoids the use of ram address 0 as
//				lcdDisplay() stops writing to lcd
//				when it encounters '\0' in a string.
//
//CG Ram Addr:	1		2		3		4		5
//
//				X		XX		XXX		XXXX	XXXXX
//				X		XX		XXX		XXXX	XXXXX
//				X		XX		XXX		XXXX	XXXXX
//				X		XX		XXX		XXXX	XXXXX
//				X		XX		XXX		XXXX	XXXXX
//				X		XX		XXX		XXXX	XXXXX
//				X		XX		XXX		XXXX	XXXXX
//
////////////////////////////////////////////////////////
void lcdBarInitialise( void )
{
	char i, buffer[8];
	
	buffer[ 7 ] = 0;
	
	for( i = 0; i < 8; i ++ )
		buffer[ i ] = BAR_ONE_PATTERN;
	lcdSetCGRam( LCD_CG_RAM_ONE, 	buffer );
	for( i = 0; i < 8; i ++ )
		buffer[ i ] = BAR_TWO_PATTERN;
	lcdSetCGRam( LCD_CG_RAM_TWO, 	buffer );
	for( i = 0; i < 8; i ++ )
		buffer[ i ] = BAR_THREE_PATTERN;
	lcdSetCGRam( LCD_CG_RAM_THREE, 	buffer );
	for( i = 0; i < 8; i ++ )
		buffer[ i ] = BAR_FOUR_PATTERN;
	lcdSetCGRam( LCD_CG_RAM_FOUR, 	buffer );
	for( i = 0; i < 8; i ++ )
		buffer[ i ] = BAR_FIVE_PATTERN;
	lcdSetCGRam( LCD_CG_RAM_FIVE, 	buffer );
}

/////////////////////////////////////////////////////////
//
//Name:			lcdSetBargraph
//
//Description:	Fills buffer with the addresses of 
//				characters from CG ram to make bargraph.
//
//				call barInitialise() first to ensure
//				correct contents of CG RAM
//
//Inputs:		value, 0 - 40. representing 40 bars
//				on an 8 char display.
//
//				barBuffer, pointer to char array.
//
//Note:			barBuffer is filled with CG RAM 
//				addresses only so don't change
//				CG RAM contents.
//
//
/////////////////////////////////////////////////////////
char lcdSetBargraph( char value, char *barBuffer )
{
	char i;
	
	barBuffer[ BARSIZ ] = '\0';		//clean up any nasty buffer

	if( value > ( BARSIZ * 5 ) )	//out of range?
		return FALSE;
	
	for( i = 0; i < BARSIZ; i ++ )	//clear bargraph buffer
		barBuffer[ i ] = SPACE;
	
	if( value == 0 )				//job done?
		return TRUE;
	
	for( i = 0; i < BARSIZ; i ++ )
	{
		if( value > 5 )
		{
			barBuffer[ i ] = BAR_FIVE;
			value -= 5;
		}
		else
		{
			barBuffer[ i ] = value;		//set final bargraph character
			return TRUE;
		}
	}

	return TRUE;

}//end lcdSetBargraph()
