/////////////////////////////////////////////////////////
//
//Name:			TXMANCHESTER.C		
//
//Author:		David McKelvie <davidmckelvie'nospam'hotmail.com>
//
//Description:	Transmitter for mohawk project, constantly 
//				transmit a number of bytes as a manchester
//				encoded data stream.
//
//Target:		12F629/12F675	uncomment PIC define below
//				16F628			comment PIC define below
//
//Version:		0.1 03/09/2003
//
//Basic Usage:	call setup();
//				call startTx();
//				wait for txDone to be TRUE
//
//Functions:	
//
//Timing:		The basic unit of time used in this program
//				is the half bit time or T. To adjust this basic
//				unit use the following formula:
//
//				T = TIME_TO_INT / PRESCALER * ( FREQ / 4) 
//
//				to calculate T for a 300us interrupt using 
//				a PRESCALER of 4, and a 4MHz clock freq
//
//				T = 300*10^-6 / 4 * 4*10^6 / 4
//				T = 75
//
//				TMR0 needs to be loaded with 256 - T to cause
//				a Timer0 overflow interrupt at TIME_TO_INT
//				TMR0 = 256 - 75
//				TMR0 = 181
//
//				To cause an idle time interrupt of 3T
//			
//				3T = T * 3
//				3T = 225
//
//				TMR0 needs to be loaded with 256 - 3T to cause
//				a Timer0 overflow interrupt at 3T
//				TMR0 = 256 - 225
//				TMR0 = 31
//
//				TIME_TO_INT		TMR0_T	TMR0_2T	TMR0_3T
//				100us			231		206		181
//				200us			206		156		106
//				300us pre=4		181		106		31
//				300us pre=8		219		181		106
//				400us			156		56		-
//				500us			131		6		-
//				600us pre=4		106		-		-
//				600us pre=8		181		106		31
//
/////////////////////////////////////////////////////////
#include<pic.h>
#include"mypic.h"

#define	_PIC_12F675		//uncomment for 12F, comment for 16F

#ifdef _PIC_12F675
__CONFIG( UNPROTECT & BORDIS & MCLRDIS & WDTDIS & INTIO );
#define	INT_OUT			GPIO0
#else
__CONFIG( INTIO & UNPROTECT & WDTDIS & LVPDIS );
#define	INT_OUT			RB7
#endif

#define	TMR0_T			181
#define	TMR0_3T			31
#define MAX_DATA		0xFF

void interrupt manchesterTx( void );
void setup( void );
void startTx( void );

struct intdata
{
	const char *buffer;		//pointer to data to be sent
	char index;				//index to buffer
	char sendStart;			//used to send start bit
	char data;				//byte to be sent
	char intCount;
	char dataBit;
	char state;
	char txDone;
}strTx;

const char txid[] = { "CHANNEL 1" };


main(void)
{
	setup();
	while( TRUE )
	{
		startTx();						//start transmitter
		while( strTx.txDone != TRUE );	//wait for transmitter
		delay( 50000 );
	}
}

//////////////////////////////////////////////////////////////////////////////////
//
//Name:			setup
//
//Description:	initialises IO, peripherals and timer
//
//////////////////////////////////////////////////////////////////////////////////
void setup( void )
{
#ifdef _PIC_12F675
	TRIS1    = 0;			//GPIO1 is output
	TRIS0    = 0;			//GPIO0 is output
#else
	CCP1CON  = 0;
	TRISB    = 0x01;
#endif
	VRCON    = 0;			//disable voltage reference
	CMCON    = 0x07;		//disconnect capture/compare
	OPTION   = 0b00000001;	//TMR0 prescaler 1:4
	INTCON   = 0b00100000;	//enable TMR0 int.
}

//////////////////////////////////////////////////////////////////////////////////
//
//Name:			startTx
//
//Description:	enables the transmitter interrupt after initialising 
//				interrupt data
//
//////////////////////////////////////////////////////////////////////////////////
void startTx( void )
{
	strTx.buffer    = txid;		//point to data to be transmitted
	strTx.index     = 0;		//point to start of data
	strTx.sendStart = 0;		//used to send start bit
	strTx.intCount  = 0;		//to keep track of ints
	INT_OUT         = 1;		//set output line to idle
	strTx.state     = 1;		//to keep track of line state
	strTx.txDone    = FALSE;
	TMR0            = TMR0_3T;	//set time to TMR0 overflow
	T0IF            = 0;		//clear TMR0 flag
	T0IE            = 1;		//enable TMR0 interrupt
	ei();						//enable interrupts
}

//////////////////////////////////////////////////////////////////////////////////
//
//Name:			manchesterTx
//
//Description:	Interrupt driven manchester encoder, sends start bit 
//				followed by data stream	up to a maximum MAX_DATA
//
//////////////////////////////////////////////////////////////////////////////////
void interrupt manchesterTx( void )
{
	if( strTx.sendStart < 2 )				//send start bit
	{
		strTx.state = strTx.sendStart++;	//set line to 0 then 1
		INT_OUT = strTx.state;
	}
	else
	{
		if( strTx.intCount == 16 )			//byte sent?
		{
			strTx.intCount = 0;				//prepare for next round of ints.
			//transmission finished?
			if( strTx.index == MAX_DATA || strTx.buffer[strTx.index] == '\0' )
			{
				strTx.txDone = TRUE;		//set flag, indicate to main
				T0IE         = 0;			//disable TMR0 interrupt
				strTx.state  = 1;			//reset line to idle
				INT_OUT      = strTx.state;	//set output to state
			}
			else
				strTx.data = strTx.buffer[strTx.index++];	//fetch next byte to send
		}
		if( strTx.intCount & 0x01 )			//every odd interrupt
		{
			strTx.state ^= 1;				//toggle state
			INT_OUT = strTx.state;			//set output to state
		}
		else								//every even interrupt
		{
			if( strTx.data & 0x80 )			//test MSB for data
				strTx.dataBit = 1;
			else
				strTx.dataBit = 0;
			if( strTx.dataBit == strTx.state )	//do we need to toggle line?	
			{
				strTx.state ^= 1;			//toggle state
				INT_OUT = strTx.state;		//set output to state
			}
			strTx.data = strTx.data << 1;	//rotate data
		}
		strTx.intCount++;					//keep track of interrupts
	}
	TMR0 = TMR0_T;							//set time to TMR0 overflow
	T0IF = 0;								//clear TMR0 flag
}

/* new untested method added 13/11/05
*  TODO: add machineState to strTx, change name from state to outputState
*  define states
void interrupt manchesterTx( void )
{
	switch( strTx . machineState )
	{
		case START_BIT_0:
			strTx . outputState = 0;
			strTx . machineState = START_BIT_1;
			break;
			
		case START_BIT_1:
			strTx . outputState = 1;
			strTx . machineState = SENDING_N;
			break;
			
		case SENDING_N:
			if( strTx . intCount & 0x01 )			//every odd interrupt
				strTx . outputState ^= 1;			//toggle state
			else									//every even interrupt
			{
				if( strTx . data & 0x80 )			//test MSB for data
					strTx . dataBit = 1;
				else
					strTx . dataBit = 0;
				if( strTx . dataBit == strTx . outputState )	//do we need to toggle line?	
					strTx . outputState ^= 1;					//toggle state
				
				strTx . data = strTx . data << 1;	//rotate data
			}
			if( ++ strTx . intCount == 15 )
				strTx . machineState = FETCH_NEXT_BYTE;
			break;
			
			
		case FETCH_NEXT_BYTE:
			strTx . intCount = 0;
			if( *(strTx . buffer) == '\0' )
			{
				strTx . txDone       = TRUE;		//set flag, indicate to main
				T0IE                 = 0;			//disable TMR0 interrupt
				strTx . outputState  = 1;			//reset line to idle
			}
			else
				strdata = *strTx . buffer ++;
			strTx . machineState = SENDING_N;
			break;
			
		default:
			//do nada
	}
	
	INT_OUT = strTx . outputState;		
	TMR0 = TMR0_T;							//set time to TMR0 overflow
	T0IF = 0;								//clear TMR0 flag
}

*/
